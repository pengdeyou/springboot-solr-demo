package com.xh;

import com.xh.demo.Application;
import com.xh.demo.bean.Article;
import com.xh.demo.consts.StringConst;
import com.xh.demo.service.ISearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.result.ScoredPage;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @Name TestSolr
 * @Description
 * @Author wen
 * @Date 2019-12-04
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class})
public class TestSolr {

    @Autowired
    private SolrTemplate solrTemplate;
    @Autowired
    private ISearchService searchService;

    @Test
    public void test1() {
        System.out.println("aaaaa");
        System.out.println("jjjjjj");
        System.out.println(searchService.search("模", 2, 10));

    }
    @Test
    public void test11() {
        searchService.findAll().forEach(System.out::println);
    }

    @Test
    public void test2() {
        System.out.println(solrTemplate.getById(StringConst.SOLR_COLLECTION1, 11111, Article.class));
    }

    @Test
    public void test3() {
        Query query = new SimpleQuery("*:*");
        Criteria criteria = new Criteria("art_summary").is("道路");
        query.addCriteria(criteria);
        ScoredPage<Article> result = solrTemplate.queryForPage(StringConst.SOLR_COLLECTION1, query, Article.class);
        List<Article> content = result.getContent();
        for (Article article : content) {
            System.out.println(article.getTitle() + "    " + article.getSummary() + "    " + article.getTags());
        }
        int totalPages = result.getTotalPages();
        int number = result.getNumber();
        long totalElements = result.getTotalElements();
        System.out.println("总页数：" + totalPages + "    总条数：" + totalElements + "    当前页：" + number);
    }

    @Test
    public void test() {
        Article article = new Article();
        article.setId(2222);
        article.setTitle("小敏说我是大帅2222");
        article.setCreateTime(new Date());
        article.setComments(2222);
        article.setFeatured(222);
        article.setFavors(222);
        article.setSummary("我成为大帅的道路很快就要出现了22222");
        article.setTags("mysql,oracle");
        article.setType(2);
        solrTemplate.saveBean(StringConst.SOLR_COLLECTION1, article);
        solrTemplate.commit(StringConst.SOLR_COLLECTION1);
    }
}
