package com.xh.demo.repository;

import com.xh.demo.bean.Article;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;

/**
 * @Name ArtcileRepository
 * @Description
 * @Author wen
 * @Date 2019-12-19
 */
public interface ArtcileRepository extends SolrCrudRepository<Article, Integer> {
}
