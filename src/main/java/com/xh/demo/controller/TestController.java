package com.xh.demo.controller;

import com.xh.demo.bean.Article;
import com.xh.demo.bean.PageResult;
import com.xh.demo.service.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Name TestController
 * @Description
 * @Author wen
 * @Date 2019-12-20
 */
@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private ISearchService searchService;

    @GetMapping("search")
    public PageResult<Article> search(@RequestParam(value = "keywords", required = false, defaultValue = "") String keywords,
                                      @RequestParam(value = "number", required = false, defaultValue = "1") int number,
                                      @RequestParam(value = "size", required = false, defaultValue = "10") int size) {
        return searchService.search(keywords, number, size);
    }
}
