package com.xh.demo.bean;

import com.xh.demo.consts.StringConst;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.io.Serializable;
import java.util.Date;

/**
 * @Name Article
 * @Description
 * @Author wen
 * @Date 2019-12-04
 */
@Data
@SolrDocument(solrCoreName = StringConst.SOLR_COLLECTION1)
public class Article implements Serializable{

    private static final long serialVersionUID = -2273571663924220337L;

    @Id
    @Field
    private Integer id;

    /**
     * 摘要信息
     */
    @Field(StringConst.ARTICLE_SUMMARY)
    private String summary;

    /**
     * 标题
     */
    @Field(StringConst.ARTICLE_TITLE)
    private String title;

    /**
     * 所属标签
     */
    @Field("art_tags")
    private String tags;

    /** 是否推荐*/
    @Field("art_featured")
    private Integer featured;

    /** 文章类型是否原创（1，0：是，否）*/
    @Field("art_type")
    private Integer type;

    /** 分类*/
    @Field("art_belong_group")
    private Integer belongGroup;

    /** 评论量*/
    @Field("art_comments")
    private Integer comments;

    /** 收藏量*/
    @Field("art_favors")
    private Integer favors;

    /** 创建时间*/
    @Field("art_create_time")
    private Date createTime;

    public String[] getTagsArray() {
        if(StringUtils.isNotBlank(this.tags)){
            return this.tags.split(StringConst.SEPARATOR);
        }
        return null;
    }

}
