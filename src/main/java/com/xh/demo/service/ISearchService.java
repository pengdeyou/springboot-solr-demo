package com.xh.demo.service;


import com.xh.demo.bean.Article;
import com.xh.demo.bean.PageResult;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;

/**
 * @Name ISearchService
 * @Description
 * @Author wen
 * @Date 2019-12-04
 */
public interface ISearchService {

    PageResult<Article> search(String keywords, long number, int size);

    void batchInsert(List<Article> articles);

    void deleteById(int id);

    Article save(Article esArticle);

    void deleteAll();

    List<Article> findAll();

    Article findById(int id);
}
