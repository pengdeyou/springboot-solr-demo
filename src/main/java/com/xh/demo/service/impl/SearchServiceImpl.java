package com.xh.demo.service.impl;

import com.xh.demo.bean.Article;
import com.xh.demo.bean.PageResult;
import com.xh.demo.consts.StringConst;
import com.xh.demo.repository.ArtcileRepository;
import com.xh.demo.service.ISearchService;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @Name SearchServiceImpl
 * @Description
 * @Author wen
 * @Date 2019-12-04
 */
@Service
public class SearchServiceImpl implements ISearchService {

    @Autowired
    private SolrTemplate solrTemplate;
    @Autowired
    private ArtcileRepository artcileRepository;

    @Override
    public PageResult<Article> search(String keywords, long number, int size) {
        if(StringUtils.isBlank(keywords)){
            return new PageResult<>();
        }
        HighlightQuery query = new SimpleHighlightQuery();
        HighlightOptions highlightOptions = new HighlightOptions();
        highlightOptions.addField(StringConst.ARTICLE_TITLE).addField(StringConst.ARTICLE_SUMMARY)
                .setSimplePrefix("<em style='color:red;'>")
                .setSimplePostfix("</em>");
        query.setHighlightOptions(highlightOptions);
        Criteria criteria = new Criteria(StringConst.ARTICLE_KEYWORDS).is(keywords);
        query.addCriteria(criteria);
        query.setOffset((number - 1) * size).setRows(size);
        HighlightPage<Article> articlePage = solrTemplate.queryForHighlightPage(StringConst.SOLR_COLLECTION1, query, Article.class);
        List<HighlightEntry<Article>> list = articlePage.getHighlighted();
        for (HighlightEntry<Article> entry : list) {
            Article article = entry.getEntity();
            List<HighlightEntry.Highlight> highlights = entry.getHighlights();
            for (HighlightEntry.Highlight highlight : highlights) {
                if (StringConst.ARTICLE_TITLE.equals(highlight.getField().getName())) {
                    article.setTitle(entry.getHighlights().get(0).getSnipplets().get(0));
                } else {
                    article.setSummary(entry.getHighlights().get(0).getSnipplets().get(0));
                }
            }
        }
        return new PageResult<>(articlePage.getTotalElements(), (int) number, articlePage.getTotalPages(), articlePage.getContent());
    }

    @Override
    public void batchInsert(List<Article> articles) {
        solrTemplate.saveBeans(StringConst.SOLR_COLLECTION1, articles);
        solrTemplate.commit(StringConst.SOLR_COLLECTION1);
    }

    @Override
    public void deleteById(int id) {
        solrTemplate.deleteByIds(StringConst.SOLR_COLLECTION1, String.valueOf(id));
        solrTemplate.commit(StringConst.SOLR_COLLECTION1);
    }

    @Override
    public Article save(Article article) {
        solrTemplate.saveBean(StringConst.SOLR_COLLECTION1, article);
        solrTemplate.commit(StringConst.SOLR_COLLECTION1);
        return article;
    }

    @Override
    public void deleteAll() {
        Query query = new SimpleQuery("*:*");
        solrTemplate.delete(StringConst.SOLR_COLLECTION1, query);
    }

    @Override
    public List<Article> findAll() {
        return Lists.newArrayList(artcileRepository.findAll());
    }

    @Override
    public Article findById(int id) {
        Optional<Article> optionalArticle = solrTemplate.getById(StringConst.SOLR_COLLECTION1, id, Article.class);
        return optionalArticle.get();
    }


}
