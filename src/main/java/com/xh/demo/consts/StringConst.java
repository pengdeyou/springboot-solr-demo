package com.xh.demo.consts;

/**
 * @Name StringConsts
 * @Description
 * @Author wen
 * @Date 2019-12-04
 */
public interface StringConst {

    String ARTICLE_SUMMARY = "art_summary";

    String ARTICLE_TITLE = "art_title";

    String ARTICLE_KEYWORDS = "art_keywords";

    String SOLR_COLLECTION1 = "collection1";

    String SEPARATOR = ",";

}
